const moviesData = require('./movies.json')
const express = require('express')
const bodyparser = require('body-parser')

//console.log(moviesData)

const app = express()
const port = 3000

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
  })

app.get('/movies', function(request,response){
    response.status(200).send({ moviesData, message: "Success" });
    
})

app.get('/movies/:name', function(request,response){
  var results = [];
  var searchField = "Title";
  var searchVal = request.params.name;
  for (var i=0 ; i < moviesData.length ; i++)
  {
      comparacion= String(moviesData[i][searchField]);
      if (comparacion.search(searchVal)!=-1) {
          results.push(moviesData[i]);
      }
  }
  
  if (results.length==0) {
    response.status(200).send("No se encontró el título buscado")
  }
  else{
    response.status(200).send(results)}
})


app.get('/movies/rating/:classifier/:order', function(request,response){
  switch(request.params.classifier){
  case "rotten" :
    
    if(request.params.order=="asc"){
      
      var results = [];
      var searchField = "Rotten Tomatoes Rating";

      for (var i=0 ; i < moviesData.length ; i++)
      { 
        results.push(moviesData[i]);
      }
      //results.sort(sortObjects);
      response.send( results )
    }
    else if(request.params.order=="desc"){

    }
    else{
      
    }
    break;

  case "imdb" :
    console.log("imdb")
    break;
  
  default:
    response.status(500).send("Hubo un error al procesar los datos, intente nuevamente")
  }
})

